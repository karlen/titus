    #!/usr/bin/env bash
    sudo apt-get update
    # Install MySQL
    echo mysql-server-5.1 mysql-server/root_password password 123456 | sudo debconf-set-selections
    echo mysql-server-5.1 mysql-server/root_password_again password 123456 | sudo debconf-set-selections
    sudo apt-get install -y mysql-server
    # Install Apache2
    sudo apt-get install -y apache2
    # Install Apache2 mod rewrite
    sudo a2enmod rewrite
    # Install PHP 5.3
    sudo apt-get install -y php5 libapache2-mod-php5
    # Install PHP Extensions
    sudo apt-get install -y php5-intl php-apc php5-gd php5-curl php5-mysql
    # Reload apache configuration
    sudo /etc/init.d/apache2 reload
    sudo rm -rf /var/www
    sudo ln -fs /vagrant /var/www

