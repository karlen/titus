<?php
class smartyTpl
{
    private static $i_smarty;

    public static function loadSmarty($templateFolder, $data = null, $template='index', $method = 'display')
    {
        if (!isset(self::$i_smarty)) {
            $smarty = new Smarty();
            $smarty->setCompileDir(CACHE_DIR);
            $smarty->setCacheDir(CACHE_DIR);
            self::$i_smarty = $smarty;
        }
        if($data != null) {
            foreach($data as $key =>$value) {
                self::$i_smarty->assign($key,$value);
            }
        }
        return self::$i_smarty->$method($templateFolder.TEMPLATES.$template.TPL);
    }
}