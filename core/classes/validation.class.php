<?php
class validation
{
    public static $regex = Array(
        'date' => "^[0-9]{4}[-/][0-9]{1,2}[-/][0-9]{1,2}\$",
        'amount' => "^[-]?[0-9]+\$",
        'number' => "^[-]?[0-9,]+\$",
        'alpha_num' => "^[0-9a-zA-Z ,.-_\\s\?\!]{5,20}+\$",
        'not_empty' => "[a-z0-9A-Z]+",
        'words' => "^[A-Za-z]+[A-Za-z \\s]{2,3}\$",
        'phone' => "^[0-9]{8,9}\$",
        'zip_code' => "^[1-9][0-9]{3}[a-zA-Z]{2}\$",
        'plate' => "^([0-9a-zA-Z]{2}[-]){2}[0-9a-zA-Z]{2}\$",
        'price' => "^[0-9.,]*(([.,][-])|([.,][0-9]{2}))?\$",
        '2digit_opt' => "^\d+(\,\d{2})?\$",
        '2digit_force' => "^\d+\,\d\d\$",
        'anything' => "^[\d\D]{1,}\$"
    );

    private $validation, $sanitation, $mandatory, $errors, $corrects, $fields;

    public function __construct($validation = array(), $mandatory = array(), $sanitation = array())
    {
        $this->validation = $validation;
        $this->sanitation = $sanitation;
        $this->mandatory = $mandatory;
        $this->errors = array();
        $this->corrects = array();
    }
    /**
     * Validates an array of items (if needed) and returns true or false
     *
     */
    public function validate($items)
    {
        $this->fields = $items;
        $haveFailures = false;
        foreach ($items as $key => $val) {
            if ((strlen($val) == 0 || array_search($key, $this->validation) === false) && array_search($key, $this->mandatory) === false) {
                $this->corrects[] = $key;
                continue;
            }
            $result = self::validateItem($val, $this->validation[$key]);
            if ($result === false) {
                $haveFailures = true;
                $this->addError($key, $this->validation[$key]);
            } else {
                $this->corrects[] = $key;
            }
        }


        return (!$haveFailures);
    }

    public static function validateItem($var, $type)
    {
        if (array_key_exists($type, self::$regex)) {
            $returnValue = filter_var($var, FILTER_VALIDATE_REGEXP, array("options" => array("regexp" => '!' . self::$regex[$type] . '!i'))) !== false;
            return ($returnValue);
        }
        $filter = false;
        switch ($type) {
            case 'email':
                $var = substr($var, 0, 254);
                $filter = FILTER_VALIDATE_EMAIL;
                break;
            case 'int':
                $filter = FILTER_VALIDATE_INT;
                break;
            case 'boolean':
                $filter = FILTER_VALIDATE_BOOLEAN;
                break;
            case 'ip':
                $filter = FILTER_VALIDATE_IP;
                break;
            case 'url':
                $filter = FILTER_VALIDATE_URL;
                break;
        }
        return ($filter === false) ? false : filter_var($var, $filter) !== false ? true : false;
    }

    public function addError($field, $type = 'string')
    {
        $this->errors[$field] = $type;
    }
    /**
     *
     *    Adds unvalidated class to thos elements that are not validated. Removes them from classes that are.
     */
    public function getScript($block)
    {
        if (!empty($this->errors)) {
            $errors = array();
            foreach ($this->errors as $key => $val) {
                $errors[] = "$('{$block} input[name={$key}]').addClass('error')";
            }
            $output = implode(';', $errors).';';
        }
        return ($output);
    }

    public function sanitize($items)
    {
        foreach ($items as $key => $val) {
            if (array_search($key, $this->sanitation) === false && !array_key_exists($key, $this->sanitation)) continue;
            $items[$key] = self::sanitizeItem($val, $this->validation[$key]);
        }
        return ($items);
    }

    public static function sanitizeItem($var, $type)
    {
        $flags = NULL;
        switch ($type) {
            case 'url':
                $filter = FILTER_SANITIZE_URL;
                break;
            case 'int':
                $filter = FILTER_SANITIZE_NUMBER_INT;
                break;
            case 'float':
                $filter = FILTER_SANITIZE_NUMBER_FLOAT;
                $flags = FILTER_FLAG_ALLOW_FRACTION | FILTER_FLAG_ALLOW_THOUSAND;
                break;
            case 'email':
                $var = substr($var, 0, 254);
                $filter = FILTER_SANITIZE_EMAIL;
                break;
            case 'string':
            default:
                $filter = FILTER_SANITIZE_STRING;
                $flags = FILTER_FLAG_NO_ENCODE_QUOTES;
                break;
        }
        $output = filter_var($var, $filter, $flags);
        return ($output);
    }
}