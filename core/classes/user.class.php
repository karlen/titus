<?php
class user
{
    private static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function userShortData()
    {
        if ($this->userLoggedIn()) {
            $us_token = $_SESSION['USER'];
            $user =  db::getInstance()->get_table('users', ' us_token = "' . $us_token . '" ');
            $user['login'] = 'yes';
        } else {
            $user = array(
                'id' => 0,
                'login' => 'no',
                'us_token' => 'user_session-'.$_COOKIE['PHPSESSID']
            );
        }
        return $user;
    }

    public function userLoggedIn()
    {
        if (isset($_SESSION['USER']) && $_SESSION['USER'] != '') {
            $usr = explode(';', hashPi::getInstance()->unCompressPi($_SESSION['USER']));
            $us_mail = $usr[0];
            $us_password = $usr[1];
            $us_token = $_SESSION['USER'];

            if ($userResult = db::getInstance()->get_table('users', 'us_mail = "' . $us_mail . '"  AND us_password = "' . $us_password . '"  AND us_token = "' . $us_token . '"  AND us_active = 1')) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function loginUser($fields)
    {
        $us_password = $fields['us_password'];
        $us_mail = $fields['us_mail'];
        $us_password = hashPi::getInstance()->compressPi($us_password);

        if ($userResult = db::getInstance()->get_table('users', 'us_mail = "' . $us_mail . '"  AND us_password = "' . $us_password . '"  AND us_active = 1')) {
            $us_token = hashPi::getInstance()->compressPi($us_mail . ';' . $us_password . ';' . time());
            $_SESSION['USER'] = $us_token;
            db::getInstance()->update("users", array('us_token' => $us_token), $userResult[0]['id']);
            return true;
        } else {
            return false;
        }
    }


    public function registrationUser($fields)
    {
        $fields['us_active'] = 1;
        $fields['us_token'] = '0';
        $us_mail = $fields['us_mail'];
        $fields['us_password'] = hashPi::getInstance()->compressPi($fields['us_password']);

        if (db::getInstance()->get_table('users', 'us_mail = "' . $us_mail . '" ')) {
            return false;
        } else {
            if (db::getInstance()->insert('users', $fields)) {
                return true;
            } else {
                return false;
            }
        }
    }
}