<?php
class component {

    protected
        $redact = '0',
        $component,
        $page,
        $settings,
        $meta,
        $blocks,
        $modules,
        $plugins,
        $view,
        $cache,
        $externalScripts,
        $externalStyles;

    public function __construct($redact,$component,$page)
    {
        $this->redact = $redact;
        $this->component = $component;
        $this->page = $page;
        $this->settings = json_decode(file_get_contents(COMPONENTS_PATH.$this->component.INDEX_JSON),true);
        $this->meta = $this->settings['meta'];
        $this->blocks = $this->settings['blocks'];
        $this->modules = $this->settings['modules'];
        $this->plugins = $this->settings['plugins'];
        $this->view = $this->settings['view'];
        $this->cache = $this->settings['cache'];
        $this->externalScripts = $this->settings['externalScripts'];
        $this->externalStyles = $this->settings['externalStyles'];
    }

    public function printComponent()
    {
        if (!page::getInstance()->isAjax) {
            echo "<!DOCTYPE html><html>";
            echo "<head>";
            $this->printHead();
            $this->printStyles();
            echo "</head>";
            echo "<body>";
            $this->printBody();
            echo "</body>";
            $this->printScripts();
            echo "</html>";
        } else {
            $this->printAjax();
        }
    }


    protected function printHead()
    {
        echo '<title>'.htmlspecialchars($this->meta['title']).'</title>';
        echo '<meta http-equiv="Content-type" content="text/html; charset=utf-8" />';
        echo '<meta name="keywords" content="'.htmlspecialchars($this->meta['keywords']).'" />';
        echo '<meta name="description" content="'.htmlspecialchars($this->meta['description']).'"/>';
        echo '<meta name="viewport" content="width=device-width, user-scalable=no">';
    }

    protected function getStyles()
    {
        $css  = "@redact:'".$this->redact."';";
        $css  .= "@component:'".$this->component."';";
        $css  .= "@page:'".$this->page."';";
        foreach(glob(GLOB_CSS) as $file) {
            $css .= file_get_contents($file);
        }
        foreach($this->externalStyles as $file) {
            $css .= file_get_contents($file);
        }
        foreach(glob(COMPONENTS_PATH.$this->component.CSS) as $file) {
            $css .= file_get_contents($file);
        }
        foreach ($this->modules as $value) {
            foreach(glob(MODULES_PATH.$value.CSS) as $file) {
                $css .= file_get_contents($file);
            }
        }
        foreach ($this->plugins as $value) {
            foreach(glob(PLUGINS_PATH.$value.CSS) as $file) {
                $css .= file_get_contents($file);
            }
        }
        return cssMin::getInstance()->run($css);
    }

    protected function printStyles()
    {
        $cssMinFile = MIN_CSS_PATH.$this->component.MIN_CSS;
        if (!file_exists($cssMinFile)) {
            $Css = fopen($cssMinFile, "w") or die(FILE_WRITE_ERROR);
            fwrite($Css, $this->getStyles());
            fclose($Css);
        } else {
            if ($this->cache) {
                $Css = fopen($cssMinFile, "w+") or die(FILE_OVERWRITE_ERROR);
                fwrite($Css, $this->getStyles());
                fclose($Css);
            }
        }
        echo '<link rel="stylesheet" type="text/css" href="'.SITE.$cssMinFile.'"/>';
    }

    protected function  printBody()
    {

        foreach ($this->blocks as  $value) {
            echo "<div class='" . $value . "_block' >";
            echo "<div class='" . $value . "_inner_block'>";
            $this->print_Modules($this->view,$value);
            echo "</div>";
            echo "</div>";
        }
    }

    public function print_Modules($view,$position)
    {
        foreach ($view[$position] as $value) {
            $module = MODULE . $value;
            $getInstance = GET_INSTANCE;
            $moduleFunc = MODULE_FUNC;
            $module::$getInstance()->$moduleFunc();
        }
    }

    protected function getScripts()
    {
        $script = "__COMPONENT__ = '".$this->component."';";
        $script .= "__PAGE__ = '".$this->page."';";
        $script .= "__US_L__ = '".user::getInstance()->userLoggedIn()."';";
        foreach(glob(GLOB_JS) as $file) {
            $script .= file_get_contents($file);
        }
        foreach($this->externalScripts as $file) {
            $script .= file_get_contents($file);
        }
        foreach(glob(COMPONENTS_PATH.$this->component.JS) as $file) {
            $script .= file_get_contents($file);
        }
        foreach ($this->modules as $value) {
            foreach(glob(MODULES_PATH.$value.JS) as $file) {
                $script .= file_get_contents($file);
            }
        }
        foreach ($this->plugins as $value) {
            foreach(glob(PLUGINS_PATH.$value.JS) as $file) {
                $script .= file_get_contents($file);
            }
        }
        return $script;
//        return jsMin::getInstance()->minify($script);
    }

    protected function printScripts()
    {
        $jsMinFile = MIN_JS_PATH.$this->component.MIN_JS;
        if (!file_exists($jsMinFile)) {
            $Js = fopen($jsMinFile, "w") or die(FILE_WRITE_ERROR);
            fwrite($Js, $this->getScripts());
            fclose($Js);
        } else {
            if($this->cache) {
                $Js = fopen($jsMinFile, "w+") or die(FILE_OVERWRITE_ERROR);
                fwrite($Js, $this->getScripts());
                fclose($Js);
            }
        }
        echo '<script type="text/javascript" src="'.SITE.$jsMinFile.'"></script>';
    }



    protected function printAjax()
    {

        $rule = page::getInstance()->seoLink;
        if ($rule[0] == $this->component) {
            $controller = $rule[1];
            $getInstance = GET_INSTANCE;
            if (in_array($controller, $this->modules)) {
                $moduleName = MODULE.$controller;
                $do = $rule[2];
                $data = $_POST;
                $moduleName::$getInstance()->$do($data);
            } else if(in_array($controller,$this->plugins)){
                $pluginName = PLUGIN.$controller;
                $do = $rule[2];
                $data = $_POST;
                $pluginName::$getInstance()->$do($data);
            }
        }
    }
}