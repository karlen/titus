<?php
class config
{
    private static $instance = null;
    private static $config = array();
    public $user;

    public static function getInstance()
    {
        if (self::$instance === null) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function __get($name)
    {
        return self::$config[$name];
    }

    private function __construct()
    {
        mb_internal_encoding("UTF-8");
        self::$config = self::getDefaultConfig();
        setlocale(LC_ALL, "en_US.UTF-8");
        return true;
    }

    public static function getDefaultConfig()
    {
        $cfg = array(
            'db_host' => 'localhost',
            'db_base' => 'titus',
            'db_user' => 'root',
            'db_pass' => '123456',
        );
        foreach ($cfg as $key => $value) {
            $cfg[$key] = stripslashes($value);
        }
        return $cfg;
    }

    public function run(){
        $this->setConstants();
        spl_autoload_register(function ($class_name) {
            if(strpos($class_name, SMARTY) !== false) {
                if(strpos($class_name, SMARTY_INTERNAL) !== false) {
                    require_once SMARTY_PLUGINS_PATH.strtolower($class_name) . PHP;
                } elseif($class_name = SMARTY) {
                    require_once SMARTY_CLASS_PATH.$class_name.CLASS_PHP;
                }
            } elseif(strpos($class_name, PLUGIN) !== false) {
                require_once PLUGINS_PATH.str_replace('plugin_', '', $class_name).INDEX_PHP;
            } elseif(strpos($class_name, MODULE) !== false) {
                require_once MODULES_PATH.str_replace('module_', '', $class_name).INDEX_PHP;
            } else {
                require_once CORE_CLASSES_PATH.$class_name.CLASS_PHP;
            }
        });
        $this->setUser();
        page::getInstance()->printPage();
    }

    protected function setUser(){
        $this->user = user::getInstance()->userShortData();
        define('USER_ID',$this->user['id']);
        define('USER_LOGIN',$this->user['login']);
        define('USER_KEY',$this->user['us_token']);
    }

    protected function setConstants() {
        define("PATH", '/vagrant');
        define("SITE", 'http://'.$_SERVER['HTTP_HOST'].'/');
        define('LANGUAGE', 'US');
        define("PLUGIN", 'plugin_');
        define("MODULE", 'module_');
        define("COMPONENT", '_Component');
        define("INDEX_PHP", '/index.php');
        define("INDEX_JSON", '/index.json');
        define("INDEX_JS", '/js/index.js');
        define("INDEX_CSS", '/css/index.css');
        define("INDEX_TPL", '/templates/index.tpl');
        define("TEMPLATES", '/templates/');
        define("CLASS_PHP", '.class.php');
        define("PHP", '.php');
        define("MIN_CSS", '.min.css');
        define("MIN_JS", '.min.js');
        define("GLOB_CSS", 'includes/assets/css/*.css');
        define("CSS", '/css/*.css');
        define("GLOB_JS", 'includes/assets/js/*.js');
        define("JS", '/js/*.js');
        define("TPL", '.tpl');
        define("SMARTY", 'Smarty');
        define("SMARTY_INTERNAL", 'Smarty_Internal');
        define("SMARTY_PLUGINS_PATH", PATH.'/includes/smarty/libs/sysPlugins/');
        define("SMARTY_CLASS_PATH", PATH.'/includes/smarty/libs/');
        define("CACHE_DIR", PATH.'/includes/cache/');
        define("PLUGINS_PATH", PATH.'/plugins/');
        define("MODULES_PATH", PATH.'/modules/');
        define("CORE_CLASSES_PATH", PATH.'/core/classes/');
        define("COMPONENTS_PATH", PATH.'/components/');
        define("MIN_CSS_PATH", 'includes/assets/css/min/');
        define("MIN_JS_PATH", 'includes/assets/js/min/');
        define("XML_REQUEST", 'xmlhttprequest');
        define("FILE_WRITE_ERROR", 'Unable to open file');
        define("FILE_OVERWRITE_ERROR", 'Unable to overwrite file');
        define("GET_INSTANCE","getInstance");
        define("MODULE_FUNC","printModule");
        define("MODULE_REDACT_FUNC","redactModule");
        define("ADMIN","admin");
        define("MAIN","main");
    }
}
config::getInstance()->run();