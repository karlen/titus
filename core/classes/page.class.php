<?php
class page
{
    protected static $instance;

    public static function getInstance()
    {
        if (self::$instance === null) {

            self::$instance = new self;
        }
        return self::$instance;
    }

    public $redact,$component,$page,$seoLink,$isMobile,$isAjax;

    protected function __construct()
    {
        session_start();
        $this->seoLink = (isset($_GET['page']) && $_GET['page'] != '') ? explode("/", $this->formatString($_GET['page'])) : false;
        if($this->seoLink != false) {
            $this->redact = ($this->seoLink[0] == ADMIN) ? true : false;
            $this->component = (!$this->redact) ? $this->seoLink[0] : $this->seoLink[1];
            if(!$this->redact) {
                $this->page = isset($this->seoLink[1]) ? $this->seoLink[1] : MAIN;
            } else {
                $this->page = isset($this->seoLink[2]) ? $this->seoLink[2] : MAIN;
            }
        } else {
            $this->redact = false;
            $this->component = MAIN;
            $this->page = MAIN;
        }

        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == XML_REQUEST) {
            $this->isAjax = true;
        }
    }

    protected function formatString($string)
    {
        $string = trim($string);
        $string = stripslashes($string);
        $string = htmlspecialchars($string);
        $string = preg_replace("/[^a-zA-Z0-9-_\/\s]/", "", $string);
        return $string;
    }

    public function not_found()
    {

    }


    public function make_static_map($points, $mapStyles = false, $is_print,  $reduce_len=false,$reduce_count=false){
        $split = '%7C';
        $mapPath = "";
        if(!$mapStyles) {
            $mapStyles   ="&style=feature:all{$split}element:all{$split}visibility:off";
            $mapStyles	.="&style=feature:landscape{$split}element:geometry.fill{$split}color:0xC0E8E8{$split}visibility:on";
            $mapStyles	.="&style=feature:water{$split}element:geometry.fill{$split}color:0xE9E5DC{$split}visibility:on";
            $mapStyles	.="&style=feature:all{$split}element:labels.text.fill{$split}color:0x282D33{$split}visibility:on";
            $mapStyles	.="&style=feature:all{$split}element:labels.text.stroke{$split}color:0xffffff{$split}visibility:on";
            $mapStyles	.="&style=feature:road{$split}element:geometry.fill{$split}color:0x64B5F6{$split}visibility:on";
            $mapStyles	.="&style=feature:transit{$split}element:geometry.fill{$split}color:0x3C90BE{$split}weight:1{$split}visibility:on";
        }

        $grp_points = array();
        $grps = array();
        $url = array();
        $max_len = 0;
        $width = 640;   //max 640 :(
        $height = 640;  //max 640 :(
        $marker_accuracy = 2;   //Lat lng to 4 decimal places minimum, 3 would be less accurate
        $path = '';
        $url[] = 'http://maps.googleapis.com/maps/api/staticmap?';
        $url[] = '&size='.$width.'x'.$height;
        $url[] = $mapStyles;
        $url[] = '&sensor=false';
        $url[] = '&markers=icon:http://wpresidence.net/wp-content/uploads/2015/08/housessales.png%7C';
        if($reduce_count){  //Last resort to shortening this
            array_splice($points, ceil(count($points)/2), 1);
        }
        foreach($points as $i => $point){
            if($reduce_len){
                $point['lat'] = number_format($point['lat'], $reduce_len, '.', '');
                $points[$i]['lat'] = $point['lat'];
                $point['lng'] = number_format($point['lng'], $reduce_len, '.', '');
                $points[$i]['lng'] = $point['lng'];
            }else{
                $t_len = max(strlen($point['lat']),strlen($point['lng']));
                if($t_len>$max_len){
                    $max_len = $t_len;
                }
            }
            $grps[] = array($point['lat'],$point['lng']);
        }
        $grps = $this->remove_duplicate_points($grps);
        foreach($grps as $grp){
            $grp_points[] = implode(',',$grp);
            $path .= $split.implode(',',$grp);
        }
        $url[] = implode('|',$grp_points);
        $url = implode('',$url);
        if(strlen($url) > 2048){
            if($max_len>$marker_accuracy){
                return($this->make_static_map($points,$max_len-1,false));
            }else{
                return($this->make_static_map($points,false,true));
            }
        }else{
            $urlLen = strlen($url);
            $pathLen = strlen($mapPath);
            if( ($urlLen + $pathLen) < 2048){
                $url = $url.$mapPath;
            }
            return($url);
        }
    }


    public function remove_duplicate_points($points){
        $points = array_map('serialize', $points);
        $points = array_unique($points);
        return(array_map('unserialize', $points));
    }


    public function getStaticMapImage($points, $mapStyles = false, $is_print = false){
        $mapUrl = $this->make_static_map($points, $mapStyles, $is_print);
        $imgPath = PATH.'/includes/assets/uploads';
        $imgName = '/map/'.md5(json_encode($points, true).$is_print).'.png';
        $imgPath = $imgPath.$imgName;
        if(!file_exists($imgPath)) {
            $img = file_get_contents($mapUrl);
            $mapImg = fopen($imgPath, "w") or die("Unable to open file!");
            fwrite($mapImg, $img);
            fclose($mapImg);
        }
        return '/includes/assets/uploads'.$imgName;
    }

    public function printPage()
    {
        $component = new component($this->redact,$this->component,$this->page);
        $component->printComponent();
    }
}