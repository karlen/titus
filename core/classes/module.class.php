<?php
class module
{
    protected
        $name,
        $data = null,
        $template = 'index',
        $method='display';

    protected static $instance;

    public static function getInstance()
    {
        $child_class =  get_called_class();
        if (!self::$instance || self::$instance != $child_class) {
            self::$instance = new $child_class();
        }
        return self::$instance;
    }

    private function __construct()
    {
        $this->name = get_called_class();
    }

    protected function getModuleSettings($module)
    {
        return json_decode(file_get_contents(MODULES_PATH.$module.INDEX_JSON),true);
    }

    protected function dataDecoder($data)
    {
        if(is_array($data)) {
            foreach($data as $key=>$value) {
                if(strpos($value, '{') !== false) {
                    $data[$key] = json_decode($value, true);
                } else {
                    $data[$key] = $value;
                }
            }
        } else {
            $data = json_decode($data,true);
        }
        return $data;
    }
    public function printModule()
    {
        $this->getModuleData();
        if(isset($this->data['template'])){
            $this->template = $this->data['template'];
        }
        if(isset($this->data['method'])){
            $this->method = $this->data['method'];
        }
        smartyTpl::loadSmarty($this->name,$this->data,$this->template,$this->method);
    }
}