<div class="module_sales">
    <div class="row">
        <div class="title"><h5>Latest Sales</h5></div>
        {foreach from=$sales  item=sale}
            <div class="col l3 s12 m6">

                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img src="{$sale.image}">
                        <span class="card-title">{$sale.title}</span>
                    </div>
                    <div class="card-content">
                        <div class="tabs_wrapper">
                            <div class="row">
                                <ul class="tabs">
                                    <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_sale_{$sale.id}">About</a></li>
                                    <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_sale_{$sale.id}">Info</a></li>
                                    {*<li class="tab col s2"><a class="blue-text text-lighten-2" href="#map_sale_{$sale.id}">Map</a></li>*}
                                </ul>
                            </div>
                        </div>
                        <div class="tab_content" id="about_sale_{$sale.id}">
                            <p>{$sale.about}</p>
                        </div>
                        <div class="tab_content" id="info_sale_{$sale.id}">
                            <div class="chip">
                                <img src="/includes/assets/images/icons/bathroom.png" alt="Contact Person">
                                Bathooms<span class="new badge">{$sale.bathrooms}</span>
                            </div>
                            <div class="chip">
                                <img src="/includes/assets/images/icons/bed.png" alt="Contact Person">
                                Rooms<span class="new badge">{$sale.rooms}</span>
                            </div>
                            <div class="chip">
                                <img src="/includes/assets/images/icons/home.png" alt="Contact Person">
                                {$sale.ft} ft2</span>
                            </div>
                            <div class="chip">
                                <img src="/includes/assets/images/icons/price.png" alt="Contact Person">
                                {$sale.price}</span>
                            </div>
                        </div>
                        {*<div class="tab_content" id="map_sale_{$sale.id}">*}
                            {*<div class="map" style="background-image: url({$sale.map})">*}
                            {*</div>*}
                        {*</div>*}
                    </div>
                    <div class="card-action">
                        <a class="blue-text text-lighten-2" href="#">More</a>
                        <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>