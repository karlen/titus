<?php

class module_contactUs
{
    protected
        $name = 'contactUs';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `comments` (
                `id` int(11) unsigned NOT NULL auto_increment,
                `full_name` varchar(255) NOT NULL default '',
                `email`  varchar(255) NOT NULL default '',
                `text` text  NOT NULL ,
                `is_new` int(5) unsigned NOT NULL default 1,
                `show_in_comments` int(5) unsigned NOT NULL default 0,
                PRIMARY KEY  (`id`)
        )ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        db::getInstance()->query($sql);

    }

    public function submit($data){
        $userName = ( isset($data['userName']) && !empty($data['userName'])) ? $data['userName'] : false;
        $message_email = ( isset($data['message_email']) && !empty($data['message_email'])) ? $data['message_email'] : false;
        $message_text = ( isset($data['message_text']) && !empty($data['message_text'])) ? $data['message_text'] : false;

        if(!$userName || !$message_email || !$message_text){
            echo 'false';
            return 0;
        }
        $errors = array();
        if(strlen($userName) < 15){
            $errors[] = 'userName';
        }if(strlen($message_text) < 15){
            $errors[] = 'message_text';
        }if(!filter_var($message_email, FILTER_VALIDATE_EMAIL)){
            $errors[] = 'message_email';
        }
        if(!empty($errors)){
            echo json_encode($errors);
            return 0;
        }else{
            if(db::getInstance()->insert('comments',array(
                'full_name' => $userName,
                'email' => $message_email,
                'text' => $message_text
            ))){
                echo 'true';
            }else{
                echo  'error';
            }
        }
    }

    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name);
    }
}