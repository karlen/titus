<div class="module_full_search row">

    <div class="map col l6 m6 s12">

    </div>

    <div class="search col l6 m6 s12">

        <div class="search_form_full_f_form row">

            <form id="search_form_form">
                {foreach key=key item=item from=$search}
                    <div class="input-field col l3 s12 m6">

                        <select name="{$key|lower|replace:' ':'_'|replace:'all_':''}">
                            <option value="all" selected>{$key}</option>
                            {foreach from=$item item=option}
                                <option {if $search_args[$key] == $option} selected {/if} value="{$option}">{$option}</option>
                            {/foreach}
                        </select>
                    </div>
                {/foreach}

                <div class="input-field col l3 s12 m6">
                    <select name="rooms">
                        <option {if $search_args['rooms'] == 0} selected {/if} value="all" selected>Min. Bedrooms</option>
                        <option {if $search_args['rooms'] == 1} selected {/if} value="1">1</option>
                        <option {if $search_args['rooms'] == 2} selected {/if} value="2">2</option>
                        <option {if $search_args['rooms'] == 3} selected {/if} value="3">3</option>
                        <option {if $search_args['rooms'] == 4} selected {/if} value="4">4</option>
                        <option {if $search_args['rooms'] == 5} selected {/if} value="5">5</option>
                    </select>
                </div>

                <div class="input-field col l3 s12 m6">
                    <label for="aviable_from" class="active">Aviable From</label>
                    <input type="date" name="aviable_from" value="{$search_args['aviable']}" id="aviable_from" class="aviable_from">
                </div>

                <label id="price_range_f_form">Price range&nbsp;&nbsp;  <span class="min"></span> - <span class="max"></span> </label>
                <div data-min="{$min}" data-max="{$max}" data-selected-min="{$search_args['min']}" data-selected-max="{$search_args['max']}" id="range-input" class="input_range col l6 s12 m12" style="margin-top: 31px"></div>
            </form>
        </div>

        <div class="items">
            {foreach from=$items item=item}
                <div class="col l6 s12 m12">
                    <div class="card" data-lat="{$item.map.lat}" data-lng="{$item.map.lng}">
                        <div class="card-image waves-effect waves-block waves-light">
                            <img src="{$item.image}">
                            <span class="card-title">{$item.title}</span>
                        </div>
                        <div class="card-content">
                            <div class="tabs_wrapper">
                                <div class="row">
                                    <ul class="tabs">
                                        <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_item_{$item.id}">About</a></li>
                                        <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_item_{$item.id}">Info</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="tab_content" id="about_item_{$item.id}">
                                <p>{$item.about}</p>
                            </div>
                            <div class="tab_content" id="info_item_{$item.id}">
                                <div class="chip">
                                    <img src="/includes/assets/images/icons/bathroom.png" alt="Contact Person">
                                    Bathooms<span class="new badge">{$item.bathrooms}</span>
                                </div>
                                <div class="chip">
                                    <img src="/includes/assets/images/icons/bed.png" alt="Contact Person">
                                    Rooms<span class="new badge">{$item.rooms}</span>
                                </div>
                                <div class="chip">
                                    <img src="/includes/assets/images/icons/home.png" alt="Contact Person">
                                    {$item.ft} ft2</span>
                                </div>
                                <div class="chip">
                                    <img src="/includes/assets/images/icons/price.png" alt="Contact Person">
                                    {$item.price}</span>
                                </div>
                            </div>
                        </div>
                        <div class="card-action">
                            <a class="blue-text text-lighten-2" href="#">More</a>
                            <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
                        </div>
                    </div>
                </div>
            {/foreach}
        </div>

    </div>

</div>