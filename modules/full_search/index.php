<?php

class module_full_search
{
    protected
        $name = 'full_search';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
    }

    public function getsearchdata(){
        $searchData = page::getInstance()->seoLink;
        unset($searchData[0]);
        unset($searchData[1]);
        unset($searchData[2]);
        $search = array();
        foreach ($searchData as $item) {
            $item = explode('-', $item);
            $key = $item[0];
            unset($item[0]);
            $value = implode('-', $item);
            $search[$key] = $value;
        }

        $items = $this->querySearchItems($search);

        foreach($items as $key => $value){
            $coords = explode(',', $value['map']);
            $value['map'] = array(
                'lat'=>$coords[0],
                'lng'=>$coords[1]
            );
            $items[$key] = $value;
        }

        echo json_encode($items, true);

    }

    public function getAllItems(){
        return db::getInstance()->get_table('items');
    }


    public function parse_search_data(){
        $searchData = page::getInstance()->seoLink;
        unset($searchData[0]);
        $search = array();
        foreach ($searchData as $item) {
            $item = explode('-', $item);
            $key = $item[0];
            unset($item[0]);
            $value = implode('-', $item);
            $search[$key] = $value;
        }

        return($search);
    }

    public function querySearchItems($search_args){
        $query = "   ";

        $i = 0;
        foreach($search_args as $key => $value){
            $cond = ($i == 0) ? ' ' : ' AND ';
            if(empty($value)){
                continue;
            }
            switch ($key) {
                case 'actions':
                    $query .= $cond.' action = "'.$value.'"';
                    break;
                case 'cities':
                    $query .= $cond.' city = "'.$value.'"';
                    break;
                case 'areas':
                    $query .= $cond.' area = "'.$value.'"';
                    break;
                case 'types':
                    $query .= $cond.' type = "'.$value.'"';
                    break;
                case 'rooms':
                    $query .= $cond.' rooms >= '.$value;
                    break;
//                case 'price':
//                    $query .= $cond.' price >= '.$value;
//                    break;
//                case 'aviable':
//                    $query .= $cond.' aviable ';
//                    break;

            }
            $i++;
        }


        if(strlen($query) > 5) {
            $array = db::getInstance()->get_table('items', $query);
            $array = ($array == false) ? array() : $array;
        }else {
            $array = $this->getAllItems();
        }

        return $array;
    }


    public function printModule()
    {
        $search_args = $this->parse_search_data();
        $items = $this->querySearchItems($search_args);

        foreach($search_args as $key => $value){
            switch ($key) {
                case 'actions':
                    $search_args['All Actions'] = $value;
                    unset($search_args[$key]);
                    break;
                case 'cities':
                    $search_args['All Cities'] = $value;
                    unset($search_args[$key]);
                    break;
                case 'areas':
                    $search_args['All Areas'] = $value;
                    unset($search_args[$key]);
                    break;
                case 'types':
                    $search_args['All Types'] = $value;
                    unset($search_args[$key]);
                    break;
            }
        }

        $search_args['rooms'] = empty($search_args['rooms']) ? 0 : $search_args['rooms'];
        $search_args['aviable'] = empty($search_args['aviable']) ? '' : $search_args['aviable'];
        $search_args['min'] = empty($search_args['min']) ? '' : $search_args['min'];
        $search_args['max'] = empty($search_args['max']) ? '' : $search_args['max'];
        $search_args['All Actions'] = empty($search_args['All Actions']) ? '' : $search_args['All Actions'];
        $search_args['All Cities'] = empty($search_args['All Cities']) ? '' : $search_args['All Cities'];
        $search_args['All Areas'] = empty($search_args['All Areas']) ? '' : $search_args['All Areas'];
        $search_args['All Types'] = empty($search_args['All Types']) ? '' : $search_args['All Types'];


        foreach($items as $key => $value){
            $coords = explode(',', $value['map']);
            $value['map'] = array(
                'lat'=>$coords[0],
                'lng'=>$coords[1]
            );
            $items[$key] = $value;
        }

        $search_params = array();

        $result = db::getInstance()->query('SELECT
                                              `action` as `All Actions`,
                                               `city` as `All Cities`,
                                               `area` as `All Areas`,
                                               `type` as `All Types`
                                                 FROM `items`');

        $data = db::getInstance()->fetch_all($result);
        foreach($data as $it){
            foreach($it as $key => $value){
                if(!isset($search_params[$key])){
                    $search_params[$key] = array();
                }
                if(!in_array($value, $search_params[$key])){
                    $search_params[$key][] = $value;
                }
            }
        }

        $result = db::getInstance()->query("SELECT
                                                MIN(price) AS minimum,
                                                MAX(price) AS maximum
                                            FROM `items`");

        $price = db::getInstance()->fetch_all($result);

        smartyTpl::loadSmarty(MODULES_PATH.$this->name, array(
            'search' => $search_params,
            'min' => $price[0]['minimum'],
            'max' => $price[0]['maximum'],
            'items' => $items,
            'search_args' => $search_args
        ));

    }



}