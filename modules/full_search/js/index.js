function fullSearch() {
    this.map = false;
    this.itemTemplate = '';

    this.init = function(){
        var _self = this;
        this.attachEvents();
        this.read('/modules/full_search/js/full_search_items.tpl', function(data){
            _self.itemTemplate = data;
        });

    }

    this.read = function(file, callback){
        var req = new XMLHttpRequest(),
            ret = null,
            async = true;
        req.open('GET', file, async);
        req.onreadystatechange = function () {
            if (req.readyState === 4) {
                ret = req.responseText;
                callback(ret);
            }
        };
        req.send(null);
    }

    this.setMapHeight = function(){
        var height = $(window).height() - 65;
        $('.module_full_search .map').css('height',height).stick_in_parent({
            spacer: false,
            offset_top: 65
        });
        $(window).resize(function(){
            var height = $(window).height() - 65;
            $('.module_full_search .map').css('height',height).stick_in_parent({
                spacer: false,
                offset_top: 65
            });
        });
    }

    this.onDataReceived = function(){
        this.loadMap();
        $('ul.tabs').tabs();
    }

    this.renderItems = function(sendUrl){
        var _self = this;

        $.post(sendUrl,{},function(data) {
            data = JSON.parse(data);

            $('.module_full_search .items').html('');
            $('.module_full_search .search').css('opacity', '1');

            _.each(data, function(item) {
                var html = _.template(_self.itemTemplate, {variable: 'data'})(item);
                $('.module_full_search .items').append(html);
            });
            _self.onDataReceived();
        });
    }

    this.getSearchData = function(sendUrl){
        var _self = this;
        $('.module_full_search .search').css('opacity', '0.5');
        if(!this.itemTemplate){
            this.read('modules/full_search/js/items.tpl', function(data){
                _self.itemTemplate = data;
                _self.renderItems(sendUrl);
            });
        }else {
            _self.renderItems(sendUrl);
        }
    }

    this.changeUrl = function(page, url) {
        if (typeof (history.pushState) != "undefined") {
            var obj = { Page: page, Url: url };
            history.pushState(obj, obj.Page, obj.Url);
        }
    }

    this.updateSearch = function(){
        var form = $('.module_full_search #search_form_form').serializeObject();
        var min_price = ($('#price_range_f_form .min').html()).replace(/\./g, "");
        var max_price = ($('#price_range_f_form .max').html()).replace(/\./g, "");
        form['min'] =  parseInt(min_price);
        form['max'] =  parseInt(max_price);

        var sendUrl  = '';

        $.each(form, function(k, v) {
            v = v || 'all';
            if(v !== 'all'){
                sendUrl +=  '/'+k+'-'+v;
            }
        });
        this.changeUrl('search', '/search'+sendUrl);
        this.getSearchData('/search/full_search/getsearchdata'+sendUrl);
    }

    this.attachEvents = function() {
        var _self = this;
        this.setMapHeight();
        $('.aviable_from').pickadate({
            selectYears: 16
        });
        this.loadMap();
        this.setRangeValue();
        $('.module_full_search input,select').on('change', function(){
            _self.updateSearch();
        });
        PubSub.subscribe('searchUpdate', function(){
            _self.updateSearch();
        });
    }

    this.loadMap = function(){

        var coordinates = [];
        $('.module_full_search .search .items .card').each(function(){
            var coords = {
                'latitude' : $(this).attr('data-lat'),
                'longitude' : $(this).attr('data-lng'),
            };
            coordinates.push(coords);
        });

        if(!this.map) {
            this.map =  _createMap($('.module_full_search .map').get(0), coordinates);
        }else {
            this.map.updateMarkers(coordinates);
        }

    }

    this.setRangeValue = function(){

        var _self = this;
        var slider = document.getElementById('range-input');
        var min =  parseInt( ($('#range-input').attr('data-min')).replace(/\./g, "") );
        var max =  parseInt( ($('#range-input').attr('data-max')).replace(/\./g, "") );

        var start_min = $('#range-input').attr('data-selected-min') || false;
        start_min = (start_min) ? parseInt( start_min ) : min;

        var start_max = $('#range-input').attr('data-selected-max') || false;
        start_max = (start_max) ? parseInt( start_max ) : max;

        var uiSlider = noUiSlider.create(slider, {
            start: [start_min, start_max],
            connect: true,
            step: 1000,
            range: {
                'min': min,
                'max': max
            }
        });
        uiSlider.on('update', function(e){
            $('#price_range_f_form .min').html(_self.toprice(parseInt(e[0])));
            $('#price_range_f_form .max').html(_self.toprice(parseInt(e[1])));
        });
        uiSlider.on('change', function(){
            PubSub.publish('searchUpdate');
        });
    }

    this.toprice = function(x){
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return parts.join(".");
    }
}

$('document').ready(function(){
    ( new fullSearch() ).init();
});