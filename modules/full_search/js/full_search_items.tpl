<div class="col l6 s12 m12">
    <div class="card" data-lat="<%= data.map.lat %>" data-lng="<%= data.map.lng %>">
        <div class="card-image waves-effect waves-block waves-light">
            <img src="<%= data.image %>">
            <span class="card-title"><%= data.title %></span>
        </div>
        <div class="card-content">
            <div class="tabs_wrapper">
                <div class="row">
                    <ul class="tabs">
                        <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_item_<%= data.id %>">About</a></li>
                        <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_item_<%= data.id %>">Info</a></li>
                    </ul>
                </div>
            </div>
            <div class="tab_content" id="about_item_<%= data.id %>">
                <p><%= data.about %></p>
            </div>
            <div class="tab_content" id="info_item_<%= data.id %>">
                <div class="chip">
                    <img src="/includes/assets/images/icons/bathroom.png" alt="Contact Person">
                    Bathooms<span class="new badge"><%= data.bathrooms %></span>
                </div>
                <div class="chip">
                    <img src="/includes/assets/images/icons/bed.png" alt="Contact Person">
                    Rooms<span class="new badge"><%= data.rooms %></span>
                </div>
                <div class="chip">
                    <img src="/includes/assets/images/icons/home.png" alt="Contact Person">
                    <%= data.ft %> ft2</span>
                </div>
                <div class="chip">
                    <img src="/includes/assets/images/icons/price.png" alt="Contact Person">
                    <%= data.price %></span>
                </div>
            </div>
        </div>
        <div class="card-action">
            <a class="blue-text text-lighten-2" href="#">More</a>
            <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
        </div>
    </div>
</div>