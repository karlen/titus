<div class="row">
    <div class="module_search col l10 m12 s12 push-l1">
        <div class="top blue lighten-2">Advanced Search</div>
        <form id="search_form_form">
            <div class="search_form">
                <div class="row">
                    <div class="select_form col l10 m10 s12">
                        <div class="row" style="margin-bottom: 0px ;">
                            {foreach key=key item=item from=$search}
                                <div class="input-field col l3 s6 m3">
                                    <select name="{$key|lower|replace:' ':'_'|replace:'all_':''}">
                                        <option value="all" selected>{$key}</option>
                                        {foreach from=$item item=option}
                                            <option value="{$option}">{$option}</option>
                                        {/foreach}
                                    </select>
                                </div>
                            {/foreach}
                        </div>
                        <div class="row" style="margin-top: 0px ;">
                            <div class="input-field col l3 s6 m3">
                                <select name="rooms">
                                    <option value="all" selected>Min. Bedrooms</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                </select>
                            </div>
                            <div class="input-field col l3 s6 m3">
                                <label for="aviable_from" class="active">Aviable From</label>
                                <input type="date" name="aviable" value="" id="aviable_from" class="aviable_from">
                            </div>
                            <label id="price_range_f_form">Price range&nbsp;&nbsp;  <span class="min"></span> - <span class="max"></span> </label>
                            <div data-min="{$min}" data-max="{$max}" id="range-input" class="input_range col l6 s12 m6" style="margin-top: 31px">
                            </div>
                        </div>
                    </div>
                    <div class="submit_form col l2 m2 s12"><i class="fa fa-search"></i></div>
                </div>
            </div>
        </form>
    </div>
</div>