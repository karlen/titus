<?php
class module_search
{
    protected $name = 'search';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
        module_rents::getInstance();
        module_sales::getInstance();

    }
    public function printModule()
    {

        $search_params = array();

        $result = db::getInstance()->query('SELECT
                                              `action` as `All Actions`,
                                               `city` as `All Cities`,
                                               `area` as `All Areas`,
                                               `type` as `All Types`
                                                 FROM `items`'
                                          );
        $data = db::getInstance()->fetch_all($result);
        foreach($data as $it){
            foreach($it as $key => $value){
                if(!isset($search_params[$key])){
                    $search_params[$key] = array();
                }
                if(!in_array($value, $search_params[$key])){
                    $search_params[$key][] = $value;
                }
            }
        }

        $result = db::getInstance()->query("SELECT
                                                MIN(price) AS minimum,
                                                MAX(price) AS maximum
                                            FROM `items`");

        $price = db::getInstance()->fetch_all($result);

        smartyTpl::loadSmarty(MODULES_PATH.$this->name, array(
            'search' => $search_params,
            'min' => $price[0]['minimum'],
            'max' => $price[0]['maximum'],
        ));
    }
}