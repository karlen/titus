$(document).ready(function() {
    function toprice(x){
        var parts = x.toString().split(".");
        parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, ".");
        return parts.join(".");
    }

    $('.aviable_from').pickadate({
        selectYears: 15
    });
    var slider = document.getElementById('range-input');
    var min =  parseInt( ($('#range-input').attr('data-min')).replace(/\./g, "") );
    var max =  parseInt( ($('#range-input').attr('data-max')).replace(/\./g, "") );

    noUiSlider.create(slider, {
        start: [min, max],
        connect: true,
        step: 1000,
        range: {
            'min': min,
            'max': max
        }
    }).on('update', function(e){
        $('#price_range_f_form .min').html(toprice(parseInt(e[0])));
        $('#price_range_f_form .max').html(toprice(parseInt(e[1])));
    });




    $('.module_search .search_form .submit_form').on('click', function(){

        var form = $('.module_search #search_form_form').serializeObject();
        var min_price = ($('#price_range_f_form .min').html()).replace(/\./g, "");
        var max_price = ($('#price_range_f_form .max').html()).replace(/\./g, "");
        form['min'] = (parseInt(min_price) == min) ? 'all' : parseInt(min_price);
        form['max'] = (parseInt(max_price) == max) ? 'all' : parseInt(max_price);

        var sendUrl  = '/search';

        $.each(form, function(k, v) {
            v = v || 'all';
            if(v !== 'all'){
                sendUrl +=  '/'+k+'-'+v;
            }
        });

            window.location.href = sendUrl;

    });




});
