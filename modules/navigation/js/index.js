$( document ).ready(function(){
    $(".button-collapse").sideNav();
});

$(window).scroll(function(){
    if($(window).scrollTop() >= 40 && !$('body').hasClass('header_sticked')){
        $('body').addClass('header_sticked');
    }else if($(window).scrollTop() < 40 && $('body').hasClass('header_sticked')){
        $('body').removeClass('header_sticked');
    }
});