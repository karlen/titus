<div class="module_navigation">
    <div class="logo"></div>
    <div class="navigation">
        <nav>
            <div class="nav-wrapper blue lighten-2">
                <a href="#" data-activates="mobile-demo" class="button-collapse right waves-effect waves-light"><i class="fa fa-bars"></i></a>
                <ul class="center-align hide-on-med-and-down">
                    <li class="active waves-effect waves-light"><a href="/">Home</a></li>
                    <li class="waves-effect waves-light"><a href="/search/actions-Sales">Sales</a></li>
                    <li class="waves-effect waves-light"><a href="/search/actions-Rentals">Rents</a></li>
                    <li class="waves-effect waves-light"><a href="#">About Us</a></li>
                    <li class="waves-effect waves-light"><a href="#">Contacts</a></li>
                    <li class="waves-effect waves-light"><a href="#">Help</a></li>
                </ul>
                <ul class="side-nav" id="mobile-demo">
                    <div class="mobile-logo"></div>
                    <li class="active"><a href="/">Home</a></li>
                    <li><a href="/search/actions-Sales">Sales</a></li>
                    <li><a href="/search/actions-Rentals">Rents</a></li>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Contacts</a></li>
                    <li><a href="#">Help</a></li>
                </ul>
            </div>
        </nav>
    </div>
</div>