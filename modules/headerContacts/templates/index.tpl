<div class="small_header">
    <div class="module_headerContacts">
        <div class="row">
            <div class="phone col l6 m12 s12"><i class="fa fa-phone"></i>&nbsp;&nbsp;1-847-555-5555&nbsp;&nbsp;&nbsp;</div>
            <div class="mail col l6 m12 s12"><i class="fa fa-envelope"></i>&nbsp;&nbsp;sales@yourwebsite.com</div>
        </div>
    </div>