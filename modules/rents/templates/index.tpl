<div class="module_rents">
    <div class="row">
        <div class="title"><h5>Latest Rents</h5></div>
        {foreach from=$rents  item=rent}
            <div class="col l3 s12 m6">

                <div class="card">
                    <div class="card-image waves-effect waves-block waves-light">
                        <img src="{$rent.image}">
                        <span class="card-title">{$rent.title}</span>
                    </div>
                    <div class="card-content">
                        <div class="tabs_wrapper">
                            <div class="row">
                                <ul class="tabs">
                                    <li class="tab col s2"><a class="active blue-text text-lighten-2" href="#about_rent_{$rent.id}">About</a></li>
                                    <li class="tab col s2"><a class="blue-text text-lighten-2" href="#info_rent_{$rent.id}">Info</a></li>
                                    {*<li class="tab col s2"><a class="blue-text text-lighten-2" href="#map_rent_{$rent.id}">Map</a></li>*}
                                </ul>
                            </div>
                        </div>
                        <div class="tab_content" id="about_rent_{$rent.id}">
                            <p>{$rent.about}</p>
                        </div>
                        <div class="tab_content" id="info_rent_{$rent.id}">
                            <div class="chip">
                                <img src="/includes/assets/images/icons/bathroom.png" alt="Contact Person">
                                Bathooms<span class="new badge">{$rent.bathrooms}</span>
                            </div>
                            <div class="chip">
                                <img src="/includes/assets/images/icons/bed.png" alt="Contact Person">
                                Rooms<span class="new badge">{$rent.rooms}</span>
                            </div>
                            <div class="chip">
                                <img src="/includes/assets/images/icons/home.png" alt="Contact Person">
                                {$rent.ft} ft2</span>
                            </div>
                            <div class="chip">
                                <img src="/includes/assets/images/icons/price.png" alt="Contact Person">
                                {$rent.price}</span>
                            </div>
                        </div>
                        {*<div class="tab_content" id="map_rent_{$rent.id}">*}
                            {*<div class="map" style="background-image: url({$rent.map})">*}
                            {*</div>*}
                        {*</div>*}
                    </div>
                    <div class="card-action">
                        <a class="blue-text text-lighten-2" href="#">More</a>
                        <i class="fa fa-share-alt right blue-text text-lighten-2"></i>
                    </div>
                </div>
            </div>
        {/foreach}
    </div>
</div>