<?php
class module_rents
{
    protected
        $name = 'rents',
        $data = null,
        $template = 'index',
        $method='display';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
    }

    protected function getLatestRents()
    {
        db::getInstance()->limit(4);
        $data = array('rents' => db::getInstance()->get_table('items', "action = 'Rentals' "));
        foreach($data['rents'] as $key => $value){
            $map = explode(',', $value['map']);
            if(count($map)>=2){
                $lat = $map[0];
                $lng = $map[1];
                $points = array(0 => array('lat'=>$lat, 'lng'=>$lng));
                $data['rents'][$key]['map'] = page::getInstance()->getStaticMapImage($points);
            }else{
                $data['rents'][$key]['map'] = '/includes/assets/uploads/map/no_map.png';
            }

        }
        db::getInstance()->resetConditions();
        return $data;
    }

    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name, $this->getLatestRents());
    }
}