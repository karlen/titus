<div class="module_comments">
    <div class="container">
        <div class="row">
            <div class="col l12 m12 s12">
                <h5>Client Testimonials</h5>
                {foreach from=$comments  item=comment}
                    <div class="comment col l6 m12 s12">
                        <div class="testimonial-container">
                            <div class="testimonial-image"></div>
                            <div class="testimonial-text">
                                {$comment.text}
                            </div>
                            <div class="testimonial-author-line">
                                <span class="testimonial-author">{$comment.full_name}</span>, ({$comment.email})
                            </div>
                        </div>
                    </div>
                {/foreach}
            </div>
        </div>
    </div>
</div>