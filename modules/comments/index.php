<?php

class module_comments
{
    protected
        $name = 'comments';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
        module_contactUs::getInstance();
    }

    protected function getComments(){
        $data = array();
        db::getInstance()->limit(2);
        db::getInstance()->orderBy('id', 'DESC');
        $data['comments'] = db::getInstance()->get_table('comments', 'show_in_comments = 1');
        db::getInstance()->resetConditions();
        return $data;
    }

    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name, $this->getComments());
    }
}