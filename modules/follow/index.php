<?php
class module_follow
{
    protected $name='follow';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `follows` (
                `id` int(11) unsigned NOT NULL auto_increment,
                `email` varchar(255) NOT NULL default '',
                PRIMARY KEY  (`id`)
        )ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        db::getInstance()->query($sql);

    }

    public function follow($data){
        if(!empty($data['mail']) && filter_var($data['mail'], FILTER_VALIDATE_EMAIL) ){
            $email = $data['mail'];
            $is_follow = db::getInstance()->get_field('follows', "email = '$email' ", 'email');
            if(!$is_follow) {
                if(db::getInstance()->insert('follows',array('email' => $data['mail']))){
                    echo 'true';
                }else{
                    echo  'error';
                }
            } else {
                echo 'true';
            }
        }else{
            echo 'false';
        }
    }

    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name);
    }
}