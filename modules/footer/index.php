<?php

class module_footer
{
    protected
        $name = 'footer';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
    }
    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name);
    }
}