<div class="module_footer">
    <footer class="page-footer">
        <div class="container">
            <div class="row">
                <div class="col l4 s12">
                    <h5 class="white-text">Titus Realty</h5>
                    <p class="grey-text text-lighten-4">
                        Titus Realty, Inc. has been servicing the local community for over 15 years by providing residential and commercial real estate services.
                    </p>
                </div>
                <div class="col l2  s12">
                    <h5 class="white-text">Links</h5>
                    <ul>
                        <li><a class="grey-text text-lighten-3" href="/">Home</a></li>
                        <li><a class="grey-text text-lighten-3" href="/search/actions-Sales">Sales</a></li>
                        <li><a class="grey-text text-lighten-3" href="/search/actions-Rentals">Rents</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">About Us</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Contacts</a></li>
                        <li><a class="grey-text text-lighten-3" href="#!">Help</a></li>
                    </ul>
                </div>
                <div class="col l6  s12">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3302.3638288585016!2d-118.25952594926368!3d34.137032820609264!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x80c2c0fcc7f015d9%3A0x7193d34c6946f38c!2zNzMwIFMgQ2VudHJhbCBBdmUsIEdsZW5kYWxlLCBDQSA5MTIwNCwg0KHQqNCQ!5e0!3m2!1sru!2s!4v1453646589803" width="100%" height="200px"  frameborder="0" style="border:0;margin-top:30px;" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="footer-copyright">
            <div class="container">
                © 2016 Titus Realty, Inc.
                <a class="grey-text text-lighten-4 right" href="#!">Titus Realty</a>
            </div>
        </div>
    </footer>
</div>