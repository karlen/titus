<?php

class module_banner
{
    protected
        $name = 'banner';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
    }
    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name);
    }
}