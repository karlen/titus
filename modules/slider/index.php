<?php

class module_slider
{
    protected $name = 'slider';

    protected static $instance;

    public static function getInstance()
    {
        if (!self::$instance ) {
            self::$instance = new self;
        }
        return self::$instance;
    }

    protected function __construct()
    {
        $sql = "CREATE TABLE IF NOT EXISTS `slider` (
                `id` int(11) unsigned NOT NULL auto_increment,
                `title` varchar(255) NOT NULL default '',
                `sub_title` varchar(255) NOT NULL default '',
                `image` varchar(255) NOT NULL default '',
                `url` varchar(255) NOT NULL default '',
                `caption` varchar(25) NOT NULL default 'right',
                PRIMARY KEY  (`id`)
        )ENGINE=MyISAM DEFAULT CHARSET=utf8;";
        db::getInstance()->query($sql);
    }

    protected function getSliderData(){
        return array('slider' => db::getInstance()->get_table('slider'));
    }


    public function printModule()
    {
        smartyTpl::loadSmarty(MODULES_PATH.$this->name, $this->getSliderData());
    }
}