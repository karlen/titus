<div class="module_slide">
    <div class="slider fullscreen">
        <span class="slide-button slide-prev">
            <i class="fa fa-angle-left"></i>
        </span>
        <span class="slide-button slide-next">
            <i class="fa fa-angle-right"></i>
        </span>
        <ul class="slides">
            {foreach from=$slider item=sliderItem}
                <li>
                    <img src="{$sliderItem.image}">
                    <div class="caption {$sliderItem.caption}-align">
                        <h3>{$sliderItem.title}</h3>
                        <h5 class="light grey-text text-lighten-3">{$sliderItem.sub_title}</h5>
                    </div>
                </li>
            {/foreach}
        </ul>
    </div>
</div>