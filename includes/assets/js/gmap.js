var maps = [];
window.__APP = [];
var apiUrl = 'https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&callback=';
var apiIsRequested = false;
var apiIsLoaded = false;
var clsInfoBox = 'infobox';
var clsInfoBoxNext = '.nav>.next';
var clsInfoBoxPrev = '.nav>.prev';
var icon = '/includes/assets/images/marker.png';


function getMapOptions(latLng) {
	return {
		zoom : 4,
		center : latLng,
		backgroundColor : '#f2f2f2',
		disableDefaultUI : true,
		draggable :  true,
		scrollwheel : false,
		styles : [
			{
			featureType : 'all',
			elementType : 'all',

		},
			{
			featureType : 'landscape',
			elementType : 'geometry',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#E0EFEF'
			}]
		},
			{
			featureType : 'water',
			elementType : 'geometry',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#64B5F6'
			}]
		},
			{
			featureType : 'all',
			elementType : 'labels.text.fill',
			stylers : [{
				visibility : 'on'
			},
			{
				color : '#000000'
			}]
		}, {
			featureType : 'all',
			elementType : 'labels.text.stroke',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#ffffff'
			}, {
				weight : 2
			}]
		}, {
			featureType : 'poi',
			elementType : 'labels.icon',
			stylers : [{
				visibility : 'simplified'
			}]
		}, {
			featureType : 'road',
			elementType : 'geometry.fill',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#ffffff'
			}]
		}, {
			featureType : 'poi',
			elementType : 'geometry',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#C0E8E8'
			}]
		}, {
			featureType : 'road',
			elementType : 'geometry.stroke',
			stylers : [{
				color: '#3C90BE'
			}]
		}, {
			featureType : 'transit',
			elementType : 'labels.icon',
			stylers : [{
				visibility : 'on'
			}, {
				invert_lightness : true
			}, {
				hue : '#ffffff'
			}, {
				saturation : -100
			}, {
				lightness : -21
			}, {
				gamma : 1.5
			}]
		}, {
			featureType : 'poi',
			elementType : 'labels',
			stylers : [{
				visibility : 'on'
			}]
		}, {
			featureType : 'transit',
			elementType : 'geometry.fill',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#75A7D8'
			}, {
				weight : 1
			}]
		}, {
			featureType : 'transit',
			elementType : 'geometry.fill',
			stylers : [{
				visibility : 'on'
			}, {
				color : '#75A7D8'
			}]
		}, {
			featureType : 'administrative.land_parcel',
			elementType : 'geometry',
			stylers : [{
				color: '#6698CD',
				fill: 'white'
			}]
		}, {
			featureType : 'administrative.land_parcel',
			elementType : 'labels',
			stylers : [{
				visibility : 'off'
			}]
		}, {
			featureType : 'road',
			elementType : 'labels.icon',
			stylers : [{
				visibility : 'off'
			}]
		}, {
			featureType : 'poi',
			elementType : 'labels.icon',
			stylers : [{
				visibility : 'off'
			}]
		}]
	};
}

function getCityCoordinates(city) {
	var coordinates = {
				latitude : -33.7969235,
				longitude : 150.9224326
			};
	return coordinates;
}

function CPMap(id, el, coordinates) {
	this.id = id;
	this.el = el;
	this.coordinates = coordinates || [];
	this.map = null;
	this.markers = {};
	this.geoMarker = null;
	this.isInit = false;

	this.nextId = null;
	this.prevId = null;
	this.infoBoxes = {};
	this.visibleInfoBoxes = [];
	this.activeInfoBox = null;
	return this;
}

CPMap.prototype.init = function() {
	var _self = this;
	if (apiIsLoaded) {
		this.render();
	} else {
		PubSub.subscribe('onGoogleMapApiReady', function() {
			_self.render();
		});
	}
	return this;
};

CPMap.prototype.render = function() {
	var _self = this;
	var zeroCoordinates = getCityCoordinates('sydney');
	if (zeroCoordinates) {
		var latLang = new google.maps.LatLng(zeroCoordinates.latitude, zeroCoordinates.longitude);
		this.map = new google.maps.Map(this.el, getMapOptions(latLang));
		this.renderMarkers();
		google.maps.event.addListener(this.map, 'dragend', function () {
			var center = _self.map.getCenter();
			PubSub.publish('onGoogleMapDragEnd', {
				id : _self.id,
				latitude : center.lat(),
				longitude : center.lng()
			});
		});
		google.maps.event.addListener(this.map, 'bounds_changed', function(){
			var center = _self.map.getCenter();
			PubSub.publish('onGoogleMapZoomChange', {
				id : _self.id,
				latitude : center.lat(),
				longitude : center.lng()
			});
		});
	}
	PubSub.subscribe('onWindowResize', function() {
		_self.resize();
		if (_self.activeInfoBox) {
			_self.activeInfoBox.hide();
			_self.activeInfoBox.open(_self.map, _self.activeInfoBox.marker);
			_self.activeInfoBox.$el.fadeIn();
			_self.activeInfoBox.show();
		}
	});
	this.isInit = true;
};

CPMap.prototype.renderMarkers = function() {
	var _self = this;

	_.each(this.visibleInfoBoxes, function(box) {
		box.hide();
	});

	_self.infoBoxes = {};

	_.each(this.coordinates, function(item) {
		_self.renderMarker(item);
	});

	// Try to fit all markers in the view.
	if (this.coordinates.length === 1) {
		this.map.setCenter({
			lat : parseFloat(this.coordinates[0].latitude),
			lng : parseFloat(this.coordinates[0].longitude)
		});
	} else if (this.coordinates.length > 0) {
		this.fitMarkers();
	}
};

CPMap.prototype.renderMarker = function(item) {
	var _self = this;

	var markerId = item.latitude + '' + item.longitude;
	if (this.markers[markerId]) {
		return null;
	}

	var makerIcon = icon;
	if (item.type && iconTypes[item.type]) {
		makerIcon = iconTypes[item.type];
	}

	var markerImage = new google.maps.MarkerImage(makerIcon, null, null, null, new google.maps.Size(44, 48));
	var latLang = new google.maps.LatLng(item.latitude, item.longitude);
	var marker = this.markers[markerId] = new google.maps.Marker({
		position : latLang,
		map : this.map,
		icon : markerImage
	});

	// If marker has a info box, display it.
	if (item.info && item.info !== '') {

		var infoBox = new window.InfoBox({
			alignBottom : true,
			boxClass : clsInfoBox,
			pixelOffset : new google.maps.Size(-310, -50),
			infoBoxClearance : new google.maps.Size(20, 20),
			content : item.info
		});

		// Assign marker to the infobox.
		infoBox.marker = marker;

		// Register info box to the collection.
		this.infoBoxes[item.id] = {
			box : infoBox,
			marker : marker
		};

		// Bind the 'infobox' object to its DOM element.
		var $infoBox = $(infoBox.getContent());
		$infoBox.data('x-infobox', infoBox);
		$infoBox.on('click', '.close', function() {
			$infoBox.data('x-infobox').hide();
		});

		$infoBox.on('click', clsInfoBoxNext, function(){
			_self.navigate('next');
		});

		$infoBox.on('click', clsInfoBoxPrev, function(){
			_self.navigate('prev');
		});

		// Assign jQuery element to the infobox.
		infoBox.$el = $infoBox;

		google.maps.event.addListener(marker, 'click', function () {
			// Close all visible info boxes.
			_.each(_self.visibleInfoBoxes, function(ib) {
				ib.hide();
			});

			_self.pushNextPrevId(item.id);

			// Open the new info box.
			_self.visibleInfoBoxes.push(infoBox);
			_self.activeInfoBox = infoBox;

			infoBox.open(_self.map, marker);
			infoBox.$el.fadeIn();
			infoBox.show();
		});

		// If marker has a url, open the url.
	} else if (item.url && item.url !== '') {
		google.maps.event.addListener(marker, 'click', function () {
			window.location.href = item.url;
		});
	}

	return marker;
};

CPMap.prototype.fitMarkers = function() {
	var bounds = new google.maps.LatLngBounds();

	_.each(this.markers, function(marker) {
		bounds.extend(marker.getPosition());
	});

	this.map.setCenter(bounds.getCenter());
	this.map.fitBounds(bounds);

	// Remove one zoom level to ensure no marker is on the edge.
	this.map.setZoom(this.map.getZoom() - 1);
	if (this.map.getZoom() > 15) {
		this.map.setZoom(15);
	}
};

CPMap.prototype.checkLocationInBounds = function($lat, $lng) {
	var bounds = this.map.getBounds();
	var latLng = new google.maps.LatLng($lat, $lng)
	return bounds.contains(latLng);
}

CPMap.prototype.clearMarkers = function() {
	_.each(this.markers, function(marker) {
		marker.setMap(null);
	});
	this.markers = {};
};

CPMap.prototype.updateMarkers = function(coordinates) {
	// Clear existing markers.
	this.clearMarkers();

	// Set and render new coordinates.
	this.coordinates = coordinates;
	if (apiIsLoaded) {
		this.renderMarkers();
	}
};

CPMap.prototype.addMarkers = function(coordinates) {
	var _self = this;

	_.each(coordinates, function(item) {
		var markerId = item.latitude + '' + item.longitude;
		if (_self.markers[markerId]) {
			_self.coordinates.push(item);
		}

		if (apiIsLoaded) {
			_self.renderMarker(item);
		}
	});
};

CPMap.prototype.moveTo = function(la, lo) {
	this.renderMarker({
		latitude : la,
		longitude : lo
	});

	// Focus on the target marker.
	var latLang = new google.maps.LatLng(la, lo);
	this.map.setCenter(latLang);
};

CPMap.prototype.moveToCurrentPosition = function(la, lo) {
	var markerImage = new google.maps.MarkerImage(icon, null, null, null, new google.maps.Size(28, 42));
	var latLang = new google.maps.LatLng(la, lo);
	this.map.setCenter(latLang);

	if (!this.geoMarker) {
		this.geoMarker = new google.maps.Marker({
			animation : google.maps.Animation.DROP,
			position : latLang,
			map : this.map,
			icon : markerImage
		});
	} else {
		this.geoMarker.setAnimation(google.maps.Animation.DROP);
	}
};

CPMap.prototype.resize = function() {
	if (this.map && google) {
		google.maps.event.trigger(this.map, 'resize');
	}
};

CPMap.prototype.zoomIn = function() {
	if (this.map && google) {
		this.map.setZoom(this.map.getZoom() + 1);
		$('body').trigger('zoom-change');
	}
};

CPMap.prototype.zoomOut = function() {
	if (this.map && google) {
		this.map.setZoom(this.map.getZoom() - 1);
		$('body').trigger('zoom-change');
	}
};

CPMap.prototype.zoomTo = function(zoom) {
	if (this.map && google) {
		this.map.setZoom(zoom);
		$('body').trigger('zoom-change');
	}
};

CPMap.prototype.pushNextPrevId = function(id) {
	var keys = Object.keys(this.infoBoxes);
	var index = keys.indexOf(id);
	var next = (index == (keys.length - 1)) ? 0 : (index + 1);
	var prev = (index == 0) ? (keys.length - 1) : (index - 1);

	this.nextId = keys[next];
	this.prevId = keys[prev];
}

CPMap.prototype.navigate = function(dir) {
	switch(dir) {
		case 'next':
			this.showInfoBox(this.nextId);
			break;
		case 'prev':
			this.showInfoBox(this.prevId);
			break;
	}
}

CPMap.prototype.showInfoBox = function(id) {

	var infoBox = this.infoBoxes[id];
	if (infoBox) {
		this.pushNextPrevId(id);

		infoBox = infoBox.box;

		// Close all visible info boxes.
		_.each(this.visibleInfoBoxes, function(box) {
			box.hide();
		});

		// Open the new info box.
		this.visibleInfoBoxes.push(infoBox);
		this.activeInfoBox = infoBox;
		infoBox.open(this.map, infoBox.marker);
		infoBox.$el.fadeIn();
		infoBox.show();
	}
};

CPMap.prototype.bounceMarker = function(markerId, bool) {
	if (!apiIsLoaded) {
		return;
	}

	var marker = this.markers[markerId];
	if (marker) {
		if (bool) {
			marker.setAnimation(google.maps.Animation.BOUNCE);
		} else {
			marker.setAnimation(null);
		}
	}
};

function mapIsLoaded() {
	apiIsLoaded = true;

	// Load 'infobox' module.
		// Broadcast API ready message.
		PubSub.publish('onGoogleMapApiReady');
}

function _createMap(el, coordinates) {
	var id = 'cpm-' + 0 + '-' + (+new Date());
	var map = new CPMap(id, el, coordinates).init();

	if (!apiIsLoaded && !apiIsRequested) {
		apiIsRequested = true;

		// Generate callback.
		var guid = 'fn' + (+new Date());
		window.__APP[guid] = function() {
			mapIsLoaded();
			delete window.__APP[guid];
		};

		// Load API.
		var js = document.createElement('script');
		js.type = 'text/javascript';
		js.async = true;
		js.src = apiUrl + 'window.__APP.' + guid;
		document.body.appendChild(js);
	}

	return map;
}


